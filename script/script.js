let array = ['hello', 'world', 23, '23', null];
let type = 'string';

function filterBy(dataArray, dataTypes) {
  const filteredArr = dataArray.filter(function(item){
    if (typeof (item) !== dataTypes){
      return true;
  }})
  return filteredArr;
  }

filterBy(array, type);
console.log(filterBy(array, type));